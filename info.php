<?php
include ("views/view_fncs.php");
include ("helpers/db_fncs.php");
include ("models/submission-model.php");
include ("helpers/validation_fncs.php");
showHeader();
showNavigation(); 
?>


<div class="content">
<div class='container-fluid'>
<div class='row'>
<div class='col-md-12'> 
    <div id="info-text">
    <h2>What you need to do...</h2>
</br>

<!--
<div class="col-md-3"></div>
    <div class="col-md-2">
        <h3>Step 1</h3>
<p>Take at least one picture over the next 4 weeks (e.g. places, buildings, selfies (be professional), shops, eateries, drinkeries)</p>
    </div>
    
    <div class="col-md-2">
        <h3>Step 2</h3>
<p>Click 'add submission' and follow the instructions detailed on that page</p>
    </div>
    
    <div class="col-md-2">
        <h3>Step 3</h3>
<p>View the submissions and 'like' the ones you like....functionality to follow</p>
    </div>
<div class="col-md-4"></div>
    -->

<div class="row">
    <div class="col-md-2"></div>
    
  <div class="col-sm-12 col-md-2">
    <div class="thumbnail">
        <img class="info-icon" src="images/camera.png" alt="...">
      <div class="caption">
        <h3>Step 1</h3>
        <p>Take at least one picture over the next 4 weeks (e.g. places, buildings, selfies (be professional), shops, eateries, drinkeries)</p>
      </div>
    </div>
  </div>  
    
      <div class="col-md-1"></div>
      
  <div class="col-sm-12 col-md-2">
    <div class="thumbnail">
      <img class="info-icon" src="images/map.png" alt="...">
      <div class="caption">
        <h3>Step 2</h3>
        <p>Add a submission (see below for video demonstration)</p> </br></br>
      </div>
    </div>
  </div>
        <div class="col-md-1"></div>     
  <div class="col-sm-12 col-md-2">
    <div class="thumbnail">
      <img class="info-icon" src="images/like.png" alt="...">
      <div class="caption">
        <h3>Step 3</h3>
        <p>View all the submissions and 'like' the ones you like....functionality to follow</p> </br>
      </div>
    </div>
  </div>
        
      <div class="col-md-2"></div>
    
 
 </div>
 <p class="note"> Note: Please be prepared to share, the pictures you provide may be used on social media.</p>   
    

<div class="row">
<h2>How to add a submission...</h2>
<div class="col-md-2"></div>
<div class="col-md-8">
<iframe height="500px" width="100%" src="https://unitube.hud.ac.uk/Embed.aspx?id=23682&amp;code=d4~HHvyxTtr8xQzi9fCSsmmLVlBSrPH" frameborder="0"></iframe>
</div>
<div class="col-md-2"></div>
</div> <!-- end rows -->
</div> <!-- end info-text -->
</div> <!-- end cols -->


</div> <!-- end rows -->
</div> <!-- end container -->
</div> <!-- end bg -->

<?php
showFooter();
?>
