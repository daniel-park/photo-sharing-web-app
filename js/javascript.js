var map;
var map2;
var marker;
var infowindow;
var messagewindow;
var lat;
var lng;
var f;
var theCurrentID;

//for adding submission
function initMap() {
    var huddersfield = {lat: 53.64328, lng: -1.777130};
    map = new google.maps.Map(document.getElementById('map'), {
        center: huddersfield,
        zoom: 15
    });

//creates a new info window object that retrieves the form element on clicking a marker.
    infowindow = new google.maps.InfoWindow({
        content: document.getElementById('form') 
    });

//assigns a click listener to the map with the addListener() callback function that creates a marker when the user clicks the map
    google.maps.event.addListenerOnce(map, 'click', function(event) {
        marker = new google.maps.Marker({
            position: event.latLng,
            map: map,
            draggable:true
      });

//assigns a click listener to the marker which displays an info window when the user clicks the marker
        google.maps.event.addListener(marker, 'click', function() {
            infowindow.open(map, marker);
            var f = document.getElementById('form');
            lat = this.position.lat();
            lng = this.position.lng();
            //alert(this.position);
            document.forms["mapSubForm"]["lat"].value = lat;
            document.forms["mapSubForm"]["lng"].value = lng;
            if (typeof (f) !== "null") {
                f.style.display = "block";  
            }
        });
          
// updates the lat and lng position if marker is dragged          
        google.maps.event.addListener(marker, 'dragend', function () {
            infowindow.open(map, marker);
            var f = document.getElementById('form');
            lat = this.position.lat();
            lng = this.position.lng();
            //alert(this.position);
            document.forms["mapSubForm"]["lat"].value = lat;
            document.forms["mapSubForm"]["lng"].value = lng;
            if (typeof (f) !== "null") {
                f.style.display = "block";   
            }    
        }); 
    });    
};
    
  
//for viewing existing markers on display all page
function initMap2() {
    var huddersfield = {lat: 53.64328, lng: -1.777130};
    map = new google.maps.Map(document.getElementById('map'), {
        center: huddersfield,
        zoom: 15
    });
    
//loads the data from json file resulting in markers being displayed
    map.data.loadGeoJson('../photo_share/upload/file.json');

      // global infowindow
  var infowindow = new google.maps.InfoWindow();

  // When the user mouseovers, open an infowindow
  map.data.addListener('mouseover', function(event) {
      var myHTML = event.feature.getProperty('title');
      infowindow.setContent("<div style='width:150px; text-align: center;'>"+myHTML+"</div>");
      infowindow.setPosition(event.feature.getGeometry().get());
      infowindow.setOptions({pixelOffset: new google.maps.Size(0,-30)});
      infowindow.open(map);
  });  

//takes user to submission page when marker clicked    
    map.data.addListener('click', function(event) { 
        var id = event.feature.getProperty('id');
        window.location.href = 'submission-details.php?submission_id='+id;
        });   
//map.data.setStyle(function(feature) {
//    var color = 'FF0000';
//    var symbol = '%E2%80%A2';  // dot
//
//    return /** @type {google.maps.Data.StyleOptions} */ {
//        visible: feature.getProperty('active'),
//        icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter_withshadow&chld=' + symbol + '|' + color
//    };
//});

};

//for viewing existing marker on details page
function initMap3() {
    var huddersfield = {lat: 53.64328, lng: -1.777130};
    map = new google.maps.Map(document.getElementById('map'), {
        center: huddersfield,
        zoom: 17
    });
  
    //loads the feature collection from json file
    map.data.loadGeoJson('../photo_share/upload/file.json', {}, function(features) {
    // get the feature you want:
    var feature = map.data.getFeatureById(theCurrentID);
    // hide the current set:
    map.data.setMap(null);
    // wipe out all the current features:
    for (var i = 0; i < features.length; i++) {
        map.data.remove(features[i]);
    }
    // add the feature you want back in:
    map.data.add(feature);
    
    // display it:
    map.data.setMap(map);
    // centre map to marker   
    map.setCenter(feature.getGeometry().get());

});
google.maps.event.addDomListener(window, "load", initMap3);
};


  /*
      var previous = null;
    var current = null;
    setInterval(function() {
        $.getJSON("upload/file.json", function(json) {
            current = JSON.stringify(json);            
            if (previous && current && previous !== current) {
                console.log('refresh');
                location.reload();
            }
            previous = current;
        });                       
    }, 2000);   */