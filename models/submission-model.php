<?php
function getAllSubmissions($conn)
{
	$query = "SELECT * FROM donna_submissions";
	$stmt = $conn->prepare($query);
	$stmt->execute();
	$allsubmissions=[];
	while($submission=$stmt->fetch(PDO::FETCH_OBJ))
	{
		$allsubmissions[]=$submission;
	}
	return $allsubmissions;
}
function getAllSubmissionsWithUserId($conn)
{
	$query = "SELECT * FROM donna_submissions, donna_users_submissions
        WHERE donna_submissions.submission_id = donna_users_submissions.submission_id";
	$stmt = $conn->prepare($query);
	$stmt->execute();
	$allsubmissions=[];
	while($submission=$stmt->fetch(PDO::FETCH_OBJ))
	{
		$allsubmissions[]=$submission;
	}
	return $allsubmissions;
}
function getAllSubmissionsForMap($conn)
{
	$query = "SELECT * FROM donna_submissions";
	$stmt = $conn->prepare($query);
	$stmt->execute();
	$allSubmissionsformap=[];
	while($submissionformap=$stmt->fetch(PDO::FETCH_OBJ))
	{
		$allsubmissionsformap[]=$submissionformap;
	}
        $jsonData =json_encode($allsubmissionsformap);
        $original_data = json_decode($jsonData, true);
        
        $features = array();
        foreach($original_data as $key => $value) {
            
            $features[] = array(
                'type' => 'Feature',
                'id' => intval($value['submission_id']),
                'properties' => array(   
                    'id' => intval($value['submission_id']),
                    'title' => ($value['title'])
                    ),
                
                'geometry' => array(
                     'type' => 'Point', 
                     'coordinates' => array(
                         floatval($value['lng']), 
                          floatval($value['lat'])
                     ),
                 ),
            );
        }
        
        $new_data = array(
            'type' => 'FeatureCollection',
            'features' => $features,
        );

        $final_data = json_encode($new_data, JSON_PRETTY_PRINT);
        //print_r($final_data);
        $file = '../photo_share/upload/file.json';
        file_put_contents($file, $final_data);
}  
function getSubmissionById($conn,$submission_id)
{
        $query = "SELECT DISTINCT donna_submissions.*, donna_users.*
        FROM donna_users_submissions
        JOIN donna_submissions on donna_users_submissions.submission_id = donna_submissions.submission_id
        JOIN donna_users on donna_users_submissions.user_id = donna_users.user_id
        WHERE donna_submissions.submission_id=:submission_id";
    	$stmt = $conn->prepare($query);
	$stmt->bindValue(':submission_id',$submission_id);
	$stmt->execute();
	$submission=$stmt->fetch(PDO::FETCH_OBJ);
	return $submission;
}
function insertSubmissionContent($conn,$title,$info,$image,$lat,$lng)
{ 	
        $query="INSERT INTO donna_submissions (title, info, image, lat, lng) VALUES (:title, :info, :fileToUpload, :lat, :lng)";
	$stmt=$conn->prepare($query);
	$stmt->bindValue(':title', $title);
	$stmt->bindValue(':info', $info);
        $stmt->bindValue(':fileToUpload', $image);
        $stmt->bindValue(':lat', $lat);
        $stmt->bindValue(':lng', $lng);
        $thisSubmission = $stmt->execute(); 
        $last_insert_id=$conn->lastInsertId();
        if($thisSubmission==1)
	{

		return array($last_insert_id, true);
	}else{
		return false;
	}
}
function insertSubmissionContentToJunctionTable($conn,$lastInsertId,$user_id)
{ 	
        $query="INSERT INTO donna_users_submissions (user_id, submission_id) VALUES (:user_id, :submission_id)";
        $stmt=$conn->prepare($query);
        $stmt->bindValue(':user_id', $user_id);
	$stmt->bindValue(':submission_id', $lastInsertId);
        $affected_rows = $stmt->execute(); 
        if($affected_rows==1)
	{
		return true;
	}else{
		return false;
	}
}
function deleteSubmission($conn,$submission_id)
{
	$query = "DELETE FROM donna_submissions WHERE submission_id=:submission_id";
	$stmt = $conn->prepare($query);
        $stmt->bindValue(':submission_id',$submission_id);
	$affected_rows = $stmt->execute();
        // print_r($conn->errorInfo());
	if($affected_rows==1)
	{
            return true;
	}
        else
        {	
            return false;
	}
}

function insertLike($conn,$like_id)
{
        $query="INSERT INTO donna_likes (like_id) VALUES (:like_id)";
	$stmt=$conn->prepare($query);
	$stmt->bindValue(':like_id', $like_id);
        $affected_rows = $stmt->execute(); 
	if($affected_rows==1)
	{
            return true;
	}
        else
        {	
            return false;
	}
}



//check Ad for matching credentials. If false display error, if true check for entry in our Db (checkForEntryInDb)
function checkAd($username, $password)
{
        //$data array
        $data["username"] = $username;
        $data["name"] = "Test Name";
        return $data;
}
// check for entry in out Db. If true, log in. If false, add user to Db (addUserToDb)
function checkForEntryInDb($conn, $username)
{
        $query = "SELECT user_id, name, username FROM donna_users WHERE username = :username";
        //$query = "SELECT * FROM donna_users WHERE username = :username";
	$stmt = $conn->prepare($query);
        $stmt->bindValue(':username',$username);    
	$stmt->execute();
	$thisuser=$stmt->fetch(PDO::FETCH_OBJ);
	return $thisuser;
}       
// At this point we have successful match to Ad and no match in our Db. We can now add user to out Db (addUserToDb)
function addUserToDb($conn,$name,$username)
{ 	
        $query="INSERT INTO donna_users (name, username) VALUES (:name, :username)";
	$stmt=$conn->prepare($query);
	$stmt->bindValue(':name', $name);
	$stmt->bindValue(':username', $username);
        $affected_rows = $stmt->execute();
        $last_insert_id=$conn->lastInsertId();
	if($affected_rows==1)
	{
		return array($last_insert_id, true);
	}else{
		return false;
	}
}
/*
function manageExistingFile(&$fileType, &$fileName, &$targetFile) {
        $fileNameNoExtension = pathinfo($fileName, PATHINFO_FILENAME);
        for($i = 1; $i < 10; $i++) {
                $newFileName = $fileNameNoExtension."_(".$i.").".$fileType;
                $targetDir = APP_FILES;
                $newTargetFile = $targetDir . $newFileName;
                if(!file_exists($newTargetFile)) {
                        $fileName = $newFileName;
                        $targetFile = $newTargetFile;
                        return true;
                }
        }
        return false;
}*/