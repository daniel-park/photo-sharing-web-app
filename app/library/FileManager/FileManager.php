<?php
class FileManager {
	
	public static function download($path, $extensions = array()) {
		if($fd = fopen($path, "r")) {
			$fsize = filesize($path);
			$path_parts = pathinfo($path);
			$ext = strtolower($path_parts["extension"]);
			
			$headersSet = false;
			
			foreach($extensions as $extension => $contentType) {
				if($ext == $extension) {
					header("Content-type: " . $contentType);
					header("Content-Disposition: attachment; filename=\"".$path_parts["basename"]."\"");
					$headersSet = true;
					break;
				}
			}
			
			if(!$headersSet) {
				header("Content-type: application/octet-stream");
				header("Content-Disposition: filename=\"".$path_parts["basename"]."\"");
			}
			
			header("Content-length: $fsize");
			header("Cache-control: private"); //for opening files directly
			
			while(!feof($fd)) {
				$buffer = fread($fd, 2048);
				echo $buffer;
			}
		}
		else {
			return false;
		}
		
		fclose($fd);
		return true;
	}
}
?>