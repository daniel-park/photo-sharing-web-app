<?php
include ("helpers/db_fncs.php");
include ("models/submission-model.php");
//they didn't submit the form
if(!isset($_POST["deleteSubmissionsBtn"]))
{
	header("Location:all-submissions-delete.php");
	exit;
}

$msg;
//validate
if(isset($_POST['submissions']))
{
	$submissions=$_POST['submissions'];
	$conn=getConn();
	$count=0;
	foreach ($submissions as $submission_id) 
	{   
            
                    
		if(deleteSubmission($conn,$submission_id))
		{
			$count++;
		}
                else{
                    echo "we got an error <br>";
                }
                
	}
       // print_r($submissions); 
        //echo $submission_id;
        
	$conn=NULL;
	$msg="<p class='success'>Successfully deleted $count submission/s</p>";
}else{
	$msg="<p class='success'>You need to select some submissions to delete";
}
include("views/delete-feedback-view.php")



?>
