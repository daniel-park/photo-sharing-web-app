<?php
include ("view_fncs.php");
showHeader("Delete Submissions");
showNavigation();
?>


<h2>Delete Submissions</h2>    
<div class="nudge">
<p>Any submissions you currently have will be displayed below:</p>
<?php
echo "<div class='mid-content'>";
echo "<form action='delete-submissions.php' method='POST'>";


foreach($allSubmissions as $submission )
{

   if ($_SESSION["user"]->user_id === $submission->user_id)
   {
	echo "<div>";
	echo "<label>".$submission->title . " </label>";
	echo "<input type='checkbox' value='".$submission->submission_id."' name='submissions[]'/>";
        echo "</div>";        
    }
}


echo "<input type='submit' name='deleteSubmissionsBtn' value='Delete submission/s'>";
echo "</form>";
echo "</div>";
?>
    </div>

<?php
showFooter();
?>
