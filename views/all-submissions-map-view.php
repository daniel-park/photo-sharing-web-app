<?php
include ("view_fncs.php");
showHeader("Show All Submissions On Map");
showNavigation();
?>            

<h2>View Submissions On Map</h2>
<p style="margin-left: 5%; margin-right: 5%; color: #f7f7f7">Hover over a map marker to see the submission title. Click on the map marker to go to the submission details.</p>

<div id="map" height="100%" ></div><!--onclick = "formShow()"-->
 

<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnBlYzaSIQEs8_4vVWtn_vfUeGfNv0huQ&callback=initMap2">
    </script>
   
<?php
showFooter();
?>

