<?php
include ("view_fncs.php");
showHeader("Show All Submissions");
showNavigation();
?>            

<div id="all-subs-background">
<h2>View Submissions</h2>
<?php

echo "<div class='container'>";
echo "<section id='demo'>";
//var_dump($allSubmissions);

foreach($allSubmissions as $submission){    
    
        echo "<article class='white-panel'>";
        echo "<a href='submission-details.php?submission_id=".$submission->submission_id."'>";
        echo "<img src=upload/";
        echo $submission->image;
        echo ">";
        echo "<h1>";
        echo $submission->title;
        echo "</h1>";
        echo "</a>";
        echo "</article>";

}
echo "</section>";
echo "</div>";
echo "</div>";
?>

</div><!-- end background -->
<?php
showFooter();
?>

<script src="js/pinterest_grid.js" type="text/javascript"></script>
<script>
$(document).ready(function() {
$('#demo').pinterest_grid({
                no_columns: 4,
                padding_x: 10,
                padding_y: 10,
                margin_bottom: 50,
                single_column_breakpoint: 700
});
});
</script>
