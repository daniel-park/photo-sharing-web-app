<?php
include ("view_fncs.php");
showHeader("Add a new submission");
showNavigation();
?>

<h2>Add Submission</h2>
<p style="margin-left: 5%; margin-right: 5%; color: #f7f7f7">Video demonstration can be found on the <a class="link" href="info.php">info page</a></p>
<p style="margin-left: 5%; margin-right: 5%; color: #f7f7f7">Step 1 - Click on the map to produce a map marker.</p>
<p style="margin-left: 5%; margin-right: 5%; color: #f7f7f7">Step 2 - Click on the map marker to open a form.</p>
<p style="margin-left: 5%; margin-right: 5%; color: #f7f7f7">Step 3 - Complete form and submit. </p>
<p style="margin-left: 5%; margin-right: 5%; color: #f7f7f7">Note: If you have created a map marker in the wrong location, you can simply drag it to the correct location before adding your submission.</p>
<div id="map" height="100%" ></div><!--onclick = "formShow()"-->
  <div id="form">
        <form action="add-map.php" method="POST" enctype="multipart/form-data" id="mapSubForm">
        <h2 style="color: #77a2ce">Add New Submission</h2>
        
        <label for="title" style="color: #77a2ce">Enter a title:</label>
        <input type="text" name="title" id="title" required><div id="title_error"></div>
        
        <label for="info"  style="color: #77a2ce">Enter information:</label></br>     
    <!--    <input type="text" name="info" id="info" required><div id="info_error"></div>  -->
        
        <textarea type="text" name="info" id="info" required></textarea><div id="info_error"></div>
        
        <label for="file"  style="color: #77a2ce">Attach image:</label>
        <!--<div>Attach Image</div>-->
        <div id="inputfileToUpload"><input type="file" name="fileToUpload" id="fileToUpload" required
        accept="image/bmp, image/dib, image/jpg, image/jpe, image/jpeg, image/jfif, image/png, image/gif, image/tif, image/tiff, image/ico">
        <div id="image_error"></div>
        </div>
        
        <input type="hidden" name="lat" id="lat" required><div id="map_error"></div>
        <input type="hidden" name="lng" id="lng">
        
        <input type="submit" name="submitBtn" value="Add Submission">
    </form>
    </div>
    <!--<div id="message">Location saved</div>-->
        <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnBlYzaSIQEs8_4vVWtn_vfUeGfNv0huQ&callback=initMap">
    </script>

    
   
 
<?php
showFooter();
?>
    
