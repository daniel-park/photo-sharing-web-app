<?php
function showHeader($title="Getting To Know Huddersfield") 
        
{ini_set('display_errors',1); error_reporting(E_ALL);
    session_start();
    
       // if (isset($_POST['submitLogInBtn'])) { 
        //$_SESSION['user'] = $_POST['username'];
       //    $_SESSION['user'] = $activeUser;
      //  }

            
	echo '<!DOCTYPE HTML>';
	echo '<html lang="en">';
	echo '<head>';
	echo '<title>' .$title. '</title>';
	echo '<meta http-equiv="content-type" content="text/html;charset=utf-8" />';
        echo '<meta name="viewport" content="width=device-width, initial-scale=1">';
        echo '<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">';
        echo '<link href="css/bootstrap.min.css" rel="stylesheet">';
        echo '<link href="css/stylesheet.css" rel="stylesheet">';
        echo '<script src="js/jquery-3.1.0.min.js" type="text/javascript"></script>';
        echo '<script src="js/bootstrap.min.js" type="text/javascript"></script>';
        echo '<script src="js/javascript.js" type="text/javascript"></script>';       
        echo '<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">';
	echo '</head>';
	echo '<body>';
}
function loginPageHeader(){
        session_start();   
        echo '<!DOCTYPE HTML>';
	echo '<html lang="en">';
	echo '<head>';
//	echo '<title>' .$title. '</title>';
	echo '<meta http-equiv="content-type" content="text/html;charset=utf-8" />';
        echo '<meta name="viewport" content="width=device-width, initial-scale=1">';
        echo '<link href="css/bootstrap.min.css" rel="stylesheet">';
        echo '<link href="css/loader.css" rel="stylesheet">'; //  this is difference between loginPageHeader and showHeader
        echo '<script src="js/jquery-3.1.0.min.js" type="text/javascript"></script>';
        echo '<script src="js/bootstrap.min.js" type="text/javascript"></script>';
        echo '<script src="js/javascript.js" type="text/javascript"></script>';       
        echo '<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">';
	echo '</head>';
	echo '<body>';
        
}
function showNavigation()
{     
    if(empty($_SESSION["user"]))
{
header("Location: login.php");
}       
        echo '<nav class="navbar navbar-default">';
        echo '<div class="container-fluid">';
        echo '<div class="navbar-header">';
        echo '<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false">';
        echo '<span class="sr-only">Toggle navigation</span>';
        echo '<span class="icon-bar"></span>';
        echo '<span class="icon-bar"></span>';
        echo '<span class="icon-bar"></span>';
        echo '</button>';
        echo '<a class="navbar-brand" href="index.php">Photo Sharing</a>';
        echo '</div>'; // end navbar-header
        echo '<div class="collapse navbar-collapse" id="navbar">';
        echo '<ul class="nav navbar-nav">';
        echo '<li>';    
        echo'<div class="dropdown">';
        echo'<button class="dropbtn">View Submissions</button>';
        echo'<div class="dropdown-content">';
        echo'<a href="all-submissions.php">On Grid</a>';
        echo'<a href="all-submissions-on-map.php">On Map</a>';
        echo'</div>';
        echo'</div>';
        echo'</li>';
        //    echo '<li><a href="all-submissions.php">View Submissions in Grid</a></li>';
        //    echo '<li><a href="all-submissions-on-map.php">View Submissions On Map</a></li>';
            echo '<li><a href="add-map-form.php">Add Submission</a></li>';
            echo '<li><a href="all-submissions-delete.php">Delete Submissions</a></li>';    
            echo '<li><a href="logout.php">Log Out</a></li>';     
            //echo ''.print_r($_SESSION, true);
            //   print_r($_SESSION);
            //    var_dump($_SESSION);
            //    var_dump($_SESSION["user"]);
           //     print_r($userData);         
            echo '<li id="logged-in-li"> You are logged in as: '.$_SESSION["user"]->name.' ('.$_SESSION["user"]->username.')</li>';
        echo '</ul>';
        echo '</div>'; //end collapse navbar-collapse
        echo '</div>'; //end container-fluid
        echo '</nav>';
        echo '<div class="site-content">'; 
        
}
function showFooter()
{
        echo'</div>'; //end site content 
        echo '<div class="footer">';
        echo '<p>Copyright &copy; '.date("Y").' University Of Huddersfield</p>';
        echo '</div>'; //end footer
	echo '</body>';
	echo '</html>';
}
?>