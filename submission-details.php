
<?php
include ("helpers/db_fncs.php");
include ("models/submission-model.php");
$conn=getConn();

if(isset($_GET['submission_id']))
{
	$submission_id = $_GET['submission_id'];
	$submission=getSubmissionById($conn,$submission_id);
	$conn=NULL;
	if($submission)
	{
		include("views/submission-details-view.php");
	}else{
		include("views/error-view.php");
	}
}else{
	include("views/error-view.php");
}

?>
