<?php
include ("views/view_fncs.php");
include ("helpers/db_fncs.php");
include ("models/submission-model.php");
include ("helpers/validation_fncs.php");
showHeader("Successfully Added To Map");
showNavigation();
$conn=getConn();
$allSubmissionsformap=getAllSubmissionsForMap($conn);
$conn=NULL;

//check if they've submitted the form
if(!isset($_POST["submitBtn"]))
{
	header("Location:add-map-form.php");
}
//get the form data
$title = $_POST['title'];
$info = $_POST['info'];
$lat = $_POST['lat'];
$lng = $_POST['lng'];
$orig_image=$_FILES['fileToUpload']["name"];
$image = time().$_FILES["fileToUpload"]["name"];
$user_id=$_SESSION["user"]->user_id;


//$validate the form data
$validForm=true;
$errorMsgs=[];

           
            
if(!complete($title))
{
	$validForm=false;
	$errorMsgs[]="<p>You need to enter a title</p>";
}
if(!complete($info))
{
	$validForm=false;
	$errorMsgs[]="<p>You need to enter information</p>";
}

if(!complete($image))
{
    	$validForm=false;
	$errorMsgs[]="<p>You need to add an image</p>";
}

//echo "should not see this";

if(isset($_FILES['fileToUpload'])){
            $maxsize    = 5242880;
            $allowed =  array('bmp', 'BMP', 'dib', 'DIB', 'jpg', 'JPG', 'jpe', 'JPE', 'jpeg', 'JPEG', 'jfif', 'JFIF', 'png', 'PNG' , 'gif', 'GIF', 'tif', 'TIF', 'tiff', 'TIFF', 'ico', 'ICO');
            $orig_image = $_FILES["fileToUpload"]["name"];
            $image = time().$orig_image;
            $ext = pathinfo($orig_image, PATHINFO_EXTENSION);
            $image = preg_replace('/\s+/', '_', $image); 
// The \s character class will match whitespace characters. The + quantifier is to collapse multiple whitespace to one underscore.
            if($_FILES['fileToUpload']['size'] >= $maxsize) 
                {
                $validForm=false;
                $errorMsgs[] = 'File too large. File must be less than 5 megabytes.';
                }
                
            else if(!in_array($ext,$allowed) ) {
            $validForm=false;
            $errorMsgs[]="<p>Ensure image file format is allowed.</p>";
            }else{      
            move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], APP_FILES . $image);       
  /*          echo $_FILES['fileToUpload']['size'];  */
            }
}
else if(!isset($_FILES['fileToUpload']))
{
    	$validForm=false;
	$errorMsgs[]="<p>There is a problem uploading your image</p>";
}

else{
        $validForm=false;
	$errorMsgs[]="<p>???</p>";
}
if(!complete($lat))
{
    	$validForm=false;
	$errorMsgs[]="<p>You need to add a marker</p>";
}
if(!complete($lng))
{
    	$validForm=false;
	$errorMsgs[]="<p>You need to add a marker</p>";
}
if(!$validForm)
{
	include("views/add-map-error-view.php");
	exit;
}

//Now try and insert the data
$conn=getConn();
$validCriteria=insertSubmissionContent($conn,$title,$info,$image,$lat,$lng);
//$success = ("hello");
$conn=NULL; //close the connection
if($validCriteria)
{ 
        //valid criteria [0] (last insert id)  
        $lastInsertId = $validCriteria[0];

        //valid criteria [1] (bool - true)  
        $returnTrue = $validCriteria[1];
        
        //this session's user id
        //$user_id
}else
{
	$errorMsgs[]="Problem inserting data into the database";
	include("views/add-map-error-view.php");
}    


$conn=getConn();
$validJunctionTblCriteria=insertSubmissionContentToJunctionTable($conn,$lastInsertId,$user_id);
$conn=NULL;

if ($validJunctionTblCriteria){

            include("views/add-map-confirm-view.php");

/*            echo "<strong>Title:</strong> ";
            echo ($title);
            echo "<br>";
            echo "<strong>Image: </strong>";
            echo ($image);
            echo "<br>";
            echo "<strong>dirname(__FILE__): </strong>";
            echo dirname(__FILE__);
            echo "<br>";
            echo '<strong>(APP_FILES . $_FILES["fileToUpload"]["name"]): </strong>';
            echo (APP_FILES . $_FILES["fileToUpload"]["name"]);
            echo "<br>";
            echo $orig_image; 
            echo "<br>";
            echo $image;
            echo "<br>";
            echo APP_FILES . $image;*/
  
            
}else
{
	$errorMsgs[]="Problem inserting data into the database";
	include("views/add-map-error-view.php");
}       