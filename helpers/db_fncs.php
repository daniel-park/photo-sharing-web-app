<?php

include("config/config.inc.php");

function getConn()
{
	try{
       $conn = new PDO('mysql:host='. DB_HOST .';dbname='. DB_DATA_SOURCE, DB_USERNAME, DB_PASSWORD);
       $conn->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
	}
	catch (PDOException $exception) 
	{
	echo "Oh no, there was a problem" . $exception->getMessage();
	}
	return $conn;
}

?>