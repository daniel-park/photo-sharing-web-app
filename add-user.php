<?php
include ("helpers/db_fncs.php");
include ("models/submission-model.php");
include ("helpers/validation_fncs.php");

//check if they've submitted the form

if (!isset($_POST['submitLogInBtn']))     
{ 
    header("Location:login.php");
}

//get the form data
$username = $_POST['username'];
$password = $_POST['password'];
//$validate the form data
$validForm=true;
$errorMsgs=[];

if(!complete($username))
{
        $validForm=false;
	$errorMsgs[]="<p>You need to enter a username</p>";
}
if(!complete($password))
{
        $validForm=false;
	$errorMsgs[]="<p>You need to enter a username</p>";
}
 if(!$validForm)
{
        include("views/error-view.php");
	exit;
}

// $userData contains all contents from checkAd (i.e. the $data array)
//$userData will have $userData["username"] + $userData["name"]
$userData = checkAd($username, $password);

//Now check for existing entry in our database
$conn=getConn();
$successCheck=checkForEntryInDb($conn,$username);
$conn=NULL;

if($successCheck){ //match found - can proceed
    
    include("views/login-success.php");
    $activeUser = $successCheck;
    $_SESSION['user'] = $activeUser;
    
}else{  //match not found - try adding to db
    
    $conn=getConn();
    $name = $userData["name"];  
    $successAdd=addUserToDb($conn,$name,$username);
    $conn=NULL; //close the connection
    
    if($successAdd){
         include("views/login-success.php");
        //success Add [0] (last insert id)  
        $user_id = $successAdd[0];
        //success Add [1] (bool - true)  
        $returnTrue = $successAdd[1];
        //create stdObject to match format of session used previously
        $activeUser = (object) array(
        'user_id' => $user_id, 'name' => $name, 'username' => $username
        );
        $_SESSION['user'] = $activeUser;
        
    }else{
        
        $errorMsgs[]="Problem inserting data into the database";
        include("views/error-view.php");
    }
}